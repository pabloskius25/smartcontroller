package fr.ece.edu.eceproject.gardencontroller;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import fr.ece.edu.eceproject.gardencontroller.model.Accessory;
import fr.ece.edu.eceproject.gardencontroller.model.Condition;
import fr.ece.edu.eceproject.gardencontroller.model.Device;

public class ConditionEditorActivity extends AppCompatActivity {
    private String previous;
    private Condition condition;
    private ArrayList<Device> devices;
    private ArrayList<Accessory> sensors;
    private ArrayList<String> comparators;
    private EditText editName;
    private Spinner spinDevice;
    private Spinner spinSensor;
    private Spinner spinComparator;
    private EditText editNumber;

    private Device device;
    private Accessory sensor;
    private String comparator;
    private String name;
    private int constant;

    private ConditionEditorActivity self;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_condition_editor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        previous = extras.getString("previous");
        devices = (ArrayList<Device>) getIntent().getSerializableExtra("devices");
        condition = (Condition) extras.getSerializable("condition");

        init();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.save_condition_editor);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = editName.getText().toString();
                device = devices.get(spinDevice.getSelectedItemPosition());
                comparator = comparators.get(spinComparator.getSelectedItemPosition());
                sensor = sensors.get(spinSensor.getSelectedItemPosition());
                constant = Integer.parseInt(editNumber.getText().toString());

                if(name != null && device != null && comparator != null && sensor != null && editNumber.getText() != null) {
                    condition.setName(name);
                    condition.setDevice(device);
                    condition.setComparator(comparator);
                    condition.setSensor(sensor);
                    condition.setConstant(constant);

                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("condition", condition);
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }
            }
        });
        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.cancel_condition_editor);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("condition", condition);
                setResult(RESULT_CANCELED, resultIntent);
                finish();
            }
        });
    }

    private void init() {
        self = this;
        editName = (EditText) findViewById(R.id.edit_name_cond);
        spinDevice = (Spinner) findViewById(R.id.spinner_device);
        spinSensor = (Spinner) findViewById(R.id.spinner_sensor);
        spinComparator = (Spinner) findViewById(R.id.spinner_comparator);
        editNumber = (EditText) findViewById(R.id.edit_number_condition);

        ArrayList<String> deviceNames = new ArrayList<>();
        for(Device device : devices)
            deviceNames.add(device.getFullname());
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, deviceNames);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinDevice.setAdapter(dataAdapter);

        comparators = new ArrayList<>();
        comparators.add(">");
        comparators.add(">=");
        comparators.add("==");
        comparators.add("!=");
        comparators.add("<=");
        comparators.add("<");
        final ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, comparators);
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinComparator.setAdapter(dataAdapter3);

        if(!previous.equals("new")){
            name = condition.getName();
            editName.setText(name);
            sensor = condition.getSensor();

            device = condition.getDevice();

            Device dev = device;
            int position = 0;
            boolean found = false;
            while(position < devices.size() && !found){
                if(dev.getFullname().equals(deviceNames.get(position))) found = true;
                else ++position;
            }
            if(!found) position = 0;
            spinDevice.setSelection(position);
            updateSensors(devices.get(position));

            ArrayList<Accessory> sens = dev.getSensors();
            ArrayList<String> sensorNames = new ArrayList<>();
            for(Accessory s : sens) sensorNames.add(s.getName());
            String senName = condition.getSensor().getName();
            position = 0;
            found = false;
            while(position < sensorNames.size() && !found){
                if(senName.equals(sensorNames.get(position))) found = true;
                else ++position;
            }
            if(!found) position = 0;
            spinSensor.setSelection(position);

            comparator = condition.getComparator();
            position = 0; found = false;
            while ( position < comparators.size() && !found){
                if( comparator.equals(comparators.get(position))) found = true;
                else ++position;
            }
            if(!found) position = 0;
            spinComparator.setSelection(position);

            constant = condition.getConstant();
            editNumber.setText(Integer.toString(constant));
        }
        else{
            updateSensors(devices.get(0));
            device = devices.get(0);
        }
        addListenerOnSpinnerItemSelection();
    }

    public void updateSensors(Device dev){
        sensors = dev.getSensors();
        ArrayList<String> sensorNames = new ArrayList<>();
        for(Accessory sens : sensors)
            sensorNames.add(sens.getName());
        final ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(self,
                android.R.layout.simple_spinner_item, sensorNames );
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinSensor.setAdapter(dataAdapter2);
    }

    public void addListenerOnSpinnerItemSelection() {
        spinDevice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateSensors(devices.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}
