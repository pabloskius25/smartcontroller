package fr.ece.edu.eceproject.gardencontroller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import fr.ece.edu.eceproject.gardencontroller.api.FetchDevicesAPITask;
import fr.ece.edu.eceproject.gardencontroller.api.PostConditionalAPITask;
import fr.ece.edu.eceproject.gardencontroller.api.PostRuleAPITask;
import fr.ece.edu.eceproject.gardencontroller.model.Accessory;
import fr.ece.edu.eceproject.gardencontroller.model.Condition;
import fr.ece.edu.eceproject.gardencontroller.api.FetchDevicesAPITask;
import fr.ece.edu.eceproject.gardencontroller.model.Device;
import fr.ece.edu.eceproject.gardencontroller.model.Rule;

public class ConditionListActivity extends AppCompatActivity {

    private ListView conditionList;
    private ArrayList<Condition> conditions = new ArrayList<>();
    private ConditionListActivity self;
    private ArrayList<Device> devices;
    private ArrayList<Accessory> actuators;
    private Rule rule;
    private Spinner spin;
    private Spinner spinActuator;
    private EditText editName;
    private String TAG = ConditionListActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_condition_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        init();

        conditionList = ((ListView) findViewById(R.id.conditionListView));
        conditionList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(self, ConditionEditorActivity.class);

                intent.putExtra("previous", "old");
                intent.putExtra("devices", devices);
                intent.putExtra("condition", (Serializable) conditions.get(position));
                startActivityForResult(intent, 1);
            }
        });

        Bundle extras = getIntent().getExtras();
        if (!extras.containsKey("rule")){
            rule = new Rule();
            rule.setId(-1);
        }
        else {
            rule = (Rule) extras.getSerializable("rule");
            editName = (EditText) findViewById(R.id.name_rule);
            editName.setText(rule.getFullname());
        }
        conditions = rule.getConditions();
        if(conditions == null)
            conditions = new ArrayList<>();

        this.spin = (Spinner)findViewById(R.id.spinner_dev_rule);
        this.spinActuator = (Spinner)findViewById(R.id.spinner_act_rule);
        addListenerOnSpinnerItemSelection();

        new FetchDevicesAPITask(this).execute();
        updateConditions();

        FloatingActionButton add = (FloatingActionButton) findViewById(R.id.fab); //ADD!
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(self, ConditionEditorActivity.class);
                intent.putExtra("previous", "new");
                intent.putExtra("devices", devices);
                Condition newCond = new Condition(-1);
                intent.putExtra("condition", (Serializable) newCond);
                startActivityForResult(intent, 2);
            }
        });

        FloatingActionButton save = (FloatingActionButton) findViewById(R.id.save_rule); //ADD!
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rule.setConditions(conditions);
                rule.setFullname(editName.getText().toString());
                new PostRuleAPITask(self, rule).execute();
            }
        });


    }

    public void finishOK(){
        setResult(RESULT_OK);
        finish();
    }

    public void init() {
        self = this;

    }

    public void updateConditions() {
        ConditionListAdapter clAdapter = new ConditionListAdapter(this, conditions, this);
        conditionList.setAdapter(clAdapter);
    }

    public void deleteCondition(Condition condition){
        conditions.remove(condition);
        updateConditions();
    }

    public void updateActuators(){
        actuators = rule.getDevice().getActuators();
        ArrayList<String> actNames = new ArrayList<>();
        for(Accessory act : actuators)
            actNames.add(act.getName());
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, actNames);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinActuator.setAdapter(dataAdapter);
        spinActuator.setSelection(actuators.indexOf(rule.getActuator()));
    }

    public void updateDevices(ArrayList<Device> devices) {
        this.devices = devices;
        ArrayList<String> deviceNames = new ArrayList<>();
        for(Device device : devices)
            deviceNames.add(device.getFullname());
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, deviceNames);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        for (Condition con : conditions) {
            boolean found = false;
            for (int i = 0; i < devices.size() && !found; ++i) {
                ArrayList<Accessory> devSensors = devices.get(i).getSensors();
                for (int j = 0; j < devSensors.size() && !found; ++j)
                    if (con.getSensor().getId() == devSensors.get(j).getId()) found = true;
                if (found) con.setDevice(devices.get(i));
            }
        }

        final int position = this.devices.indexOf(rule.getDevice());

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                spin.setAdapter(dataAdapter);
                updateConditions();
                spin.setSelection(position);
            }
        });
    }

    public void addListenerOnSpinnerItemSelection() {
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                rule.setDevice(devices.get(position));
                actuators = devices.get(position).getActuators();
                updateActuators();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spinActuator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                rule.setActuator(actuators.get(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (1) : {
                if (resultCode == Activity.RESULT_OK) {
                    Condition newCond = (Condition) data.getSerializableExtra("condition");
                    int index = conditions.indexOf(newCond);
                    conditions.set(index, newCond);
                    updateConditions();
                }
                break;
            }
            case (2) : {
                if (resultCode == Activity.RESULT_OK) {
                    Condition newCond = (Condition) data.getSerializableExtra("condition");
                    conditions.add(newCond);
                    updateConditions();
                }
                break;
            }
        }
    }
}