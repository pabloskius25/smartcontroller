package fr.ece.edu.eceproject.gardencontroller;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.ece.edu.eceproject.gardencontroller.api.FetchRulesAPITask;
import fr.ece.edu.eceproject.gardencontroller.model.Condition;
import fr.ece.edu.eceproject.gardencontroller.model.Rule;

public class MainActivity extends AppCompatActivity {

    private ListView rulesList;
    private ArrayList<Rule> rules;
    private String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rulesList = (ListView) findViewById(R.id.listViewRules);
        rulesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getSelf(), ConditionListActivity.class);
                intent.putExtra("rule", rules.get(position));
                Log.d("ruleERROR", rules.get(position).getConditions().toString());
                startActivityForResult(intent, 1);
            }
        });

        new FetchRulesAPITask(this).execute();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getSelf(), ConditionListActivity.class);
                intent.putExtra("rule", new Rule(-1));
                startActivityForResult(intent, 1);
            }
        });
    }


    public void updateRules(ArrayList<Rule> rules) {
        this.rules = rules;
        final RuleListAdapter adapter = new RuleListAdapter(this, R.layout.list_item, rules);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                rulesList.setAdapter(adapter);
            }
        });
    }

    public void deleteRule(Rule rule) {
        rules.remove(rule);
        updateRules(rules);
    }

    public MainActivity getSelf() {
        return this;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (1) : {
                if(resultCode == Activity.RESULT_OK)
                    new FetchRulesAPITask(this).execute();
                break;
            }
        }
    }
}
