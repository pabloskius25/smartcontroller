package fr.ece.edu.eceproject.gardencontroller.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Pau on 16/03/2016.
 */
public class Device implements Serializable {

    private int id;
    private String fullname;
    private ArrayList<Accessory> sensors;
    private ArrayList<Accessory> actuators;

    public Device() {
        sensors = new ArrayList<>();
        actuators = new ArrayList<>();
    }

    public Device(int id, String fullname, ArrayList<Accessory> sensors, ArrayList<Accessory> actuators) {
        this.id = id;
        this.fullname = fullname;
        this.sensors = sensors;
        this.actuators = actuators;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public ArrayList<Accessory> getSensors() {
        return sensors;
    }

    public void setSensors(ArrayList<Accessory> sensors) {
        this.sensors = sensors;
    }

    public ArrayList<Accessory> getActuators() {
        return actuators;
    }

    public void setActuators(ArrayList<Accessory> actuators) {
        this.actuators = actuators;
    }

    public void addSensor(Accessory sensor) {
        this.sensors.add(sensor);
    }

    public void addActuator(Accessory actuator) {
        this.actuators.add(actuator);
    }
}
