package fr.ece.edu.eceproject.gardencontroller;

import fr.ece.edu.eceproject.gardencontroller.api.DeleteConditionalAPITask;
import fr.ece.edu.eceproject.gardencontroller.model.Condition;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import fr.ece.edu.eceproject.gardencontroller.model.Condition;

/**
 * Created by oriol on 03/02/2016.
 */
public class ConditionListAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final ArrayList<Condition> conditions;
    private final ConditionListActivity CLA;

    public ConditionListAdapter(Context context, ArrayList<Condition> conditions, ConditionListActivity CLA) {
        super(context, R.layout.condition_list_item);
        this.context = context;
        this.conditions = conditions;
        this.CLA = CLA;
    }

    @Override
    public int getCount() {
        return conditions.size();
    }

    private static class PlaceHolder {

        TextView name;
        ImageButton deleteButton;
        Condition condition;

        public static PlaceHolder generate(View convertView) {
            PlaceHolder placeHolder = new PlaceHolder();
            placeHolder.name = (TextView) convertView.findViewById(R.id.condNameList);
            placeHolder.deleteButton = (ImageButton) convertView.findViewById(R.id.deleteCondition);
            return placeHolder;
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final PlaceHolder placeHolder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.condition_list_item, null);
            placeHolder = PlaceHolder.generate(convertView);
            convertView.setTag(placeHolder);
        } else {
            placeHolder = (PlaceHolder) convertView.getTag();
        }

        final Condition condition = conditions.get(position);

        placeHolder.condition = condition;
        placeHolder.name.setText(condition.getName());

        placeHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DeleteConditionalAPITask(condition.getId()).execute();
                CLA.deleteCondition(condition);
            }
        });

        return convertView;
    }
}
