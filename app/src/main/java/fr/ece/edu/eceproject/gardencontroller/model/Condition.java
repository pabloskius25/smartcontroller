package fr.ece.edu.eceproject.gardencontroller.model;

import java.io.Serializable;

import fr.ece.edu.eceproject.gardencontroller.model.Accessory;

/**
 * Created by oriol on 03/02/2016.
 */
public class Condition implements Serializable {

    private int id;
    private String name;
    private String comparator;
    private int constant;
    private Accessory sensor;
    private Device device;

    public Condition() {}

    public Condition(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComparator() {
        return comparator;
    }

    public void setComparator(String comparator) {
        this.comparator = comparator;
    }

    public int getConstant() {
        return constant;
    }

    public void setConstant(int constant) {
        this.constant = constant;
    }

    public Accessory getSensor() {
        return sensor;
    }

    public void setSensor(Accessory sensor) {
        this.sensor = sensor;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Condition condition = (Condition) o;

        return id == condition.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}