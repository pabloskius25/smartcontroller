package fr.ece.edu.eceproject.gardencontroller.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Pau on 16/03/2016.
 */
public class Rule implements Serializable{

    private int id;
    private String fullname;
    private ArrayList<Condition> conditions;
    private Device device;
    private Accessory actuator;

    public Rule() {
        conditions = new ArrayList<>();
    }

    public Rule(int id) {
        this.id = id;
    }

    public Rule(int id, String fullname, ArrayList<Condition> conditions, Device device, Accessory actuator) {
        this.id = id;
        this.fullname = fullname;
        this.conditions = conditions;
        this.device = device;
        this.actuator = actuator;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public ArrayList<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(ArrayList<Condition> conditions) {
        this.conditions = conditions;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Accessory getActuator() {
        return actuator;
    }

    public void setActuator(Accessory actuator) {
        this.actuator = actuator;
    }

    public void addCondition(Condition condition) {
        this.conditions.add(condition);
    }
}
