package fr.ece.edu.eceproject.gardencontroller.api;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import fr.ece.edu.eceproject.gardencontroller.model.Condition;
import fr.ece.edu.eceproject.gardencontroller.MainActivity;
import fr.ece.edu.eceproject.gardencontroller.model.Accessory;
import fr.ece.edu.eceproject.gardencontroller.model.Rule;

/**
 * Created by Pau on 16/03/2016.
 */
public class FetchRulesAPITask extends AsyncTask<Void,Void,Void> {

    private MainActivity context;
    private String TAG = FetchRulesAPITask.class.getSimpleName();
    private ProgressDialog progDialog;

    public FetchRulesAPITask(MainActivity context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progDialog = new ProgressDialog(context);
        progDialog.setMessage("Loading rules...");
        progDialog.setIndeterminate(false);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setCancelable(false);
        progDialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String stationJsonString = null;

        try {
            URL url = new URL("http://eceproject.byte.cat/api/rules");

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuilder buffer = new StringBuilder();
            if (inputStream == null) {
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line).append("\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            stationJsonString = buffer.toString();
        } catch (IOException e) {
            Log.e(TAG, "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attemping
            // to parse it.
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(TAG, "Error closing stream", e);
                }
            }
        }

        try {
            getRulesDataFromJSON(stationJsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progDialog.dismiss();
    }

    public void getRulesDataFromJSON(String data) throws JSONException {
        Log.d(TAG, "Json received after get: " + data);
        ArrayList<Rule> rules = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(data);
        for(int i = 0; i < jsonArray.length(); ++i) {
            JSONObject json = jsonArray.getJSONObject(i);
            Rule rule = new Rule();
            rule.setId(json.getInt("id"));
            rule.setFullname(json.getString("fullname"));
            JSONArray conditionals = json.getJSONArray("conditionals");
            for(int j = 0; j < conditionals.length(); ++j) {
                JSONObject conditionJson = conditionals.getJSONObject(j);
                Condition condition = new Condition();
                condition.setId(conditionJson.getInt("id"));
                condition.setName(conditionJson.getString("name"));
                condition.setComparator(conditionJson.getString("comparator"));
                condition.setConstant(conditionJson.getInt("constant"));
                JSONObject sensorJson = conditionJson.getJSONObject("sensor");
                Accessory sensor = new Accessory();
                sensor.setId(sensorJson.getInt("id"));
                sensor.setName(sensorJson.getString("name"));
                condition.setSensor(sensor);
                rule.addCondition(condition);
            }
            JSONObject actuatorJson = json.getJSONObject("actuator");
            Accessory actuator = new Accessory();
            actuator.setId(actuatorJson.getInt("id"));
            actuator.setName(actuatorJson.getString("name"));
            rule.setActuator(actuator);
            rules.add(rule);
        }
        context.updateRules(rules);
    }
}
