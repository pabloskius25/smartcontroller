package fr.ece.edu.eceproject.gardencontroller.api;

import android.content.ContentValues;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import fr.ece.edu.eceproject.gardencontroller.ConditionListActivity;
import fr.ece.edu.eceproject.gardencontroller.model.Condition;
import fr.ece.edu.eceproject.gardencontroller.model.Rule;

/**
 * Created by Pau on 19/03/2016.
 */
public class PostRuleAPITask extends AsyncTask<Void,Void,Void> {

    private String TAG = PostRuleAPITask.class.getSimpleName();
    private ContentValues parameters;
    private Rule rule;
    private ConditionListActivity context;

    public PostRuleAPITask(ConditionListActivity context, Rule rule) {
        this.rule = rule;
        this.context = context;
        parameters = new ContentValues();
        JSONObject json = new JSONObject();
        try {
            json.put("fullname", rule.getFullname());
            json.put("actuator", rule.getActuator().getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        parameters.put("data", json.toString());
        Log.d(TAG, "Bundle to send: " + parameters.toString());
    }

    @Override
    protected Void doInBackground(Void... params) {
        HttpURLConnection urlConnection = null;
        String resultJsonString = "";

        try {
            String urlString = "http://eceproject.byte.cat/api/rules";
            String method;

            if(rule.getId() != -1) {
                urlString += "/" + rule.getId();
                method = "PUT";
            }
            else
                method = "POST";

            URL url = new URL(urlString);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod(method);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.connect();

            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(parameters.toString());
            out.close();

            Log.d(TAG, String.valueOf(urlConnection.getResponseCode()));

//            String f = "";
//
//            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream(), "UTF-8"));
//            String line;
//            while((line = in.readLine()) != null) {
//                f += line;
//            }
//            in.close();
//
//            Log.d("Error Response: ", f);

//            Get Response
            InputStream is = urlConnection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            // StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                resultJsonString += line;
            }
            rd.close();

            handleResponse(resultJsonString);

        } catch (Exception e) {
            Log.e(TAG, "Error ", e);
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return null;
    }

    private void handleResponse(String data) throws JSONException {
        JSONObject json = new JSONObject(data);
        rule.setId(json.getInt("id"));
        Log.d(TAG, "Json received after put: " + data);
        for(int i = 0; i < rule.getConditions().size(); ++i){
            boolean last = false;
            if(i == rule.getConditions().size()-1)
                last = true;
            new PostConditionalAPITask(context, rule.getConditions().get(i), rule.getId(), last).execute();
        }
        Log.d(TAG, "Number of Conditions: " + rule.getConditions().size());
        if(rule.getConditions().size() == 0)
            context.finishOK();
    }
}
