package fr.ece.edu.eceproject.gardencontroller.api;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import fr.ece.edu.eceproject.gardencontroller.ConditionListActivity;
import fr.ece.edu.eceproject.gardencontroller.MainActivity;
import fr.ece.edu.eceproject.gardencontroller.model.Accessory;
import fr.ece.edu.eceproject.gardencontroller.model.Device;

/**
 * Created by Pau on 16/03/2016.
 */
public class FetchDevicesAPITask extends AsyncTask<Void,Void,Void> {

    private ConditionListActivity context;
    private String TAG = FetchDevicesAPITask.class.getSimpleName();
    private ProgressDialog progDialog;

    public FetchDevicesAPITask(ConditionListActivity context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progDialog = new ProgressDialog(context);
        progDialog.setMessage("Loading devices...");
        progDialog.setIndeterminate(false);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setCancelable(false);
        progDialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String stationJsonString = null;

        try {
            URL url = new URL("http://eceproject.byte.cat/api/devices");

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            stationJsonString = buffer.toString();
        } catch (IOException e) {
            Log.e(TAG, "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attemping
            // to parse it.
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(TAG, "Error closing stream", e);
                }
            }
        }

        try {
            getDevicesDataFromJSON(stationJsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progDialog.dismiss();
    }

    public void getDevicesDataFromJSON(String data) throws JSONException {
        Log.d(TAG, "Json received after get: " + data);
        ArrayList<Device> devices = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(data);
        for(int i = 0; i < jsonArray.length(); ++i) {
            JSONObject json = jsonArray.getJSONObject(i);
            Device device = new Device();
            device.setId(json.getInt("id"));
            device.setFullname(json.getString("fullname"));
            JSONArray sensors = json.getJSONArray("sensors");
            for(int j = 0; j < sensors.length(); ++j) {
                JSONObject sensorJson = sensors.getJSONObject(j);
                Accessory sensor = new Accessory();
                sensor.setId(sensorJson.getInt("id"));
                sensor.setName(sensorJson.getString("name"));
                device.addSensor(sensor);
            }
            JSONArray actuators = json.getJSONArray("actuators");
            for(int j = 0; j < actuators.length(); ++j) {
                JSONObject actuatorJson = actuators.getJSONObject(j);
                Accessory actuator = new Accessory();
                actuator.setId(actuatorJson.getInt("id"));
                actuator.setName(actuatorJson.getString("name"));
                device.addActuator(actuator);
            }
            devices.add(device);
        }
        context.updateDevices(devices);
    }
}
