package fr.ece.edu.eceproject.gardencontroller.api;

import android.content.ContentValues;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import fr.ece.edu.eceproject.gardencontroller.ConditionListActivity;
import fr.ece.edu.eceproject.gardencontroller.model.Rule;

/**
 * Created by Pau on 19/03/2016.
 */
public class DeleteRuleAPITask extends AsyncTask<Void,Void,Void> {

    private String TAG = DeleteRuleAPITask.class.getSimpleName();
    private int ruleId;

    public DeleteRuleAPITask(int ruleId) {
        this.ruleId = ruleId;
    }

    @Override
    protected Void doInBackground(Void... params) {
        HttpURLConnection urlConnection = null;

        try {
            String urlString = "http://eceproject.byte.cat/api/rules/" + ruleId;
            URL url = new URL(urlString);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("DELETE");
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.connect();

            Log.d(TAG, String.valueOf(urlConnection.getResponseCode()));

//            String f = "";
//
//            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream(), "UTF-8"));
//            String line;
//            while((line = in.readLine()) != null) {
//                f += line;
//            }
//            in.close();
//
//            Log.d("Error Response: ", f);

        } catch (Exception e) {
            Log.e(TAG, "Error ", e);
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }
}
