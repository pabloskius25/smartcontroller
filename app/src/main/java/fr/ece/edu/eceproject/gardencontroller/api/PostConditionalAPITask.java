package fr.ece.edu.eceproject.gardencontroller.api;

import android.content.ContentValues;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import fr.ece.edu.eceproject.gardencontroller.ConditionListActivity;
import fr.ece.edu.eceproject.gardencontroller.model.Condition;

/**
 * Created by Pau on 20/03/2016.
 */
public class PostConditionalAPITask extends AsyncTask<Void,Void,Void> {

    private String TAG = PostConditionalAPITask.class.getSimpleName();
    private ContentValues parameters;
    private Condition condition;
    private ConditionListActivity context;
    private boolean last;

    public PostConditionalAPITask(ConditionListActivity context, Condition condition, int ruleId, boolean last) {
        this.condition = condition;
        this.context = context;
        this.last = last;
        parameters = new ContentValues();
        JSONObject json = new JSONObject();
        try {
            json.put("name", condition.getName());
            json.put("sensor", condition.getSensor().getId());
            json.put("comparator", condition.getComparator());
            json.put("constant", condition.getConstant());
            json.put("rule", ruleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        parameters.put("data", json.toString());
        Log.d(TAG, "Bundle to send: " + parameters.toString());
    }

    @Override
    protected Void doInBackground(Void... params) {
        HttpURLConnection urlConnection = null;
        String resultJsonString = "";

        try {
            String urlString = "http://eceproject.byte.cat/api/conditionals";
            String method;

            if(condition.getId() != -1) {
                urlString += "/" + condition.getId();
                method = "PUT";
            }
            else
                method = "POST";

            URL url = new URL(urlString);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod(method);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.connect();

            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(parameters.toString());
            out.close();

            Log.d(TAG, String.valueOf(urlConnection.getResponseCode()));

//            String f = "";
//
//            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream(), "UTF-8"));
//            String line;
//            while((line = in.readLine()) != null) {
//                f += line;
//            }
//            in.close();
//
//            Log.d("Error Response: ", f);

//            Get Response
            InputStream is = urlConnection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            // StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                resultJsonString += line;
            }
            rd.close();

            handleResponse(resultJsonString);

        } catch (IOException e) {
            Log.e(TAG, "Error ", e);
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return null;
    }

    private void handleResponse(String data) throws JSONException {
        JSONObject json = new JSONObject(data);
        condition.setId(json.getInt("id"));
        Log.d(TAG, "Json received after put: " + data);
        if(last)
            context.finishOK();
    }
}
