package fr.ece.edu.eceproject.gardencontroller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.ece.edu.eceproject.gardencontroller.api.DeleteRuleAPITask;
import fr.ece.edu.eceproject.gardencontroller.model.Rule;

public class RuleListAdapter extends ArrayAdapter<Rule> {

    private ArrayList<Rule> rules;
    private MainActivity context;

    public RuleListAdapter(MainActivity context, int resource, ArrayList<Rule> rules) {
        super(context, resource, rules);
        this.rules = rules;
        this.context = context;
    }

    @Override
    public int getCount() {
        return rules.size();
    }

    private static class PlaceHolder {

        TextView fullname;
        TextView actuatorName;
        ImageButton deleteButton;
        Rule rule;

        public static PlaceHolder generate(View convertView) {
            PlaceHolder placeHolder = new PlaceHolder();
            placeHolder.fullname = (TextView) convertView.findViewById(R.id.ruleFullname);
            placeHolder.deleteButton = (ImageButton) convertView.findViewById(R.id.deleteRule);
            placeHolder.actuatorName = (TextView) convertView.findViewById(R.id.actuatorName);
            return placeHolder;
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final PlaceHolder placeHolder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.list_item, null);
            placeHolder = PlaceHolder.generate(convertView);
            convertView.setTag(placeHolder);
        } else {
            placeHolder = (PlaceHolder) convertView.getTag();
        }

        final Rule rule = rules.get(position);

        placeHolder.rule = rule;
        placeHolder.fullname.setText(rule.getFullname());
        placeHolder.actuatorName.setText(rule.getActuator().getName());

        placeHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DeleteRuleAPITask(rule.getId()).execute();
                context.deleteRule(rule);
            }
        });

        return convertView;
    }
}
