package fr.ece.edu.eceproject.gardencontroller.model;

import java.io.Serializable;

/**
 * Created by Pau on 16/03/2016.
 */
public class Accessory implements Serializable {

    private int id;
    private String name;

    public Accessory() {}

    public Accessory(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Accessory accessory = (Accessory) o;

        if (id != accessory.id) return false;
        return !(name != null ? !name.equals(accessory.name) : accessory.name != null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
